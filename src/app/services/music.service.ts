import { baseUrl } from './../shared/base-url';
import { DomSanitizer } from '@angular/platform-browser';
import { Injectable, OnInit } from '@angular/core';
import * as _ from 'lodash';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable, } from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/catch';

@Injectable()

export class MusicService implements OnInit{
  private url;
  public elm;

  constructor(private http: HttpClient, private sanitizer: DomSanitizer) {
    this.url = baseUrl() + "/music";
  } 

  ngOnInit() {

  }

  getMusic(): Observable<any> {
    return this.http.get(this.url);
  }

}
