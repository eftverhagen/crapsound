import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { baseUrl } from '../shared/base-url';

@Injectable()
export class ApiService {

  constructor(public _http: HttpClient) { 
    
  }

  submitMessage(model) {
    const url = baseUrl() + "/post-message";

    const body = {
      type: model.type,
      message: model.message,
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };

    return this._http.post(url, body, httpOptions);
  }
}
