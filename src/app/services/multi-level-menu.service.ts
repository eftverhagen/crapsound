
import {Injectable, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs/Observable';
import {Structure} from '../shared/menu-structure';
import {AppState} from '../models/app-state';
import * as _ from 'lodash';

@Injectable()
export class MultiLevelMenuService implements OnInit{
  public state;
  private order;
  public contentLeft;
  public initialWidth;
  public offset;
  public colors;
  public levelWidth;

  constructor(private store: Store<AppState>) { 
    this.order = [0];
    this.levelWidth = 240;
    this.offset = 40;
    this.colors = this.generateColors(true, 0, 400, Structure.length)
    this.state = _.map(Structure, (level, idx) => { 
      level.active = 0;
      level.offset = 0;
      level.color = this.colors[idx];
      return level;
    });
    
    this.calculateOffset();
    this.calculateContentLeft();
  }

  ngOnInit() {
    this.calculateOffset();
    this.calculateContentLeft();
  }

  getCurrentValue(): Observable<number> {
    return this.store.select(appState => appState.counter.currentValue);
  }

  calculateContentLeft() {
    this.contentLeft = _.max(_.map(this.order, (i, index) => {
      let n = index;
      return this.levelWidth + (n * this.offset);
    }));
  }

  calculateOffset(){
    let pxMap = _.map(this.order, (i, index) => {
      let n = index + 1;
      return (n * this.offset);
    });
    this.state = _.map(this.state, level => {
      _.map(this.order, levelId => {
        if(level.id === levelId){
          level.offset = this.order.indexOf(levelId) * this.offset;
        }
      });
      return level;
    });
  }

  isOverlapped(levelId) {
    let index = this.order.indexOf(levelId);
    return typeof this.order[index -1] !== 'undefined';
  }

  toggleMenu($event) {
    $event.stopPropagation();
    this.state = _.map(this.state, level => {
      if(level.id === 0) {
        level.active = 1 - level.active;
        if(level.active === 1) {
          this.order = [0];
        }
      }else{
        level.active = 0;
      }
      return level;
    });

    this.calculateOffset();
    this.calculateContentLeft();
  }

  openLevel($event, levelId) {
    $event.stopPropagation();
    this.state = _.map(this.state, level => {
      if(level.id === levelId && this.order.indexOf(level.id) === -1) {
        level.active = 1;
        this.order.unshift(levelId);
      }
      return level;
    });
    
    this.calculateOffset();
    this.calculateContentLeft();
  }

  closeUntil(levelId) {
    let stop = false;
    this.order = _.filter(this.order, id => {
      if(id === levelId){
        stop = true;
      }
      return stop;
    });

    this.state = _.map(this.state, level => {
      if(level.id === levelId) {
        level.active = 1;
      }else if(this.order.indexOf(level.id) > this.order.indexOf(levelId)){
        level.active = 1;
      }else{
        level.active = 0;
      }
      return level;
    });

    this.calculateOffset();
    this.calculateContentLeft();
  }

  generateColors(random, start, end, numOfLevels) {
    let colors = [];
    let r = 125;
    let g = 50;
    let b = 50;
    let step = _.floor(_.difference([end], [start]) / numOfLevels);
    let picks = [..._.range(start, end, step), end];
    
    let seq = [
      {r: 0, b: 0, g: +1},
      {r: -1, b: 0, g: 0},
      {r: 0, b: +1, g: 0},
      {r: 0, b: 0, g: -1},
      {r: +1, b: 0, g: 0},
      {r: 0, b: -1, g: 0},
    ];

    _.map(seq, a => {
      for(let i = 0; i <= 75; i++){
        if(i ===0) {
          colors.push({r,b,g});
        }else{
          r += a.r;
          b += a.b;
          g += a.g;
          colors.push({r,g,b});
        }
      }
    });


    if(random) {
      colors = _.map(picks, pick => colors[pick]);
    }
    return colors;
  }
}
