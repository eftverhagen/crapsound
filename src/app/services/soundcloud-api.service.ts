import { baseUrl } from './../shared/base-url';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class SoundcloudApiService {
  private clientId;
  private currentTrackId;

  constructor(private _http: HttpClient ) { 
    this.clientId = "iomhTXEZDUKzHt3KYi11vVWHwcdzI22s";
  }

  resolve(url){
    return this._http.get("https://api.soundcloud.com/resolve.json?url=" + url  + "&client_id=" + this.clientId);
  }

  publish(song){
    
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };

    const url = baseUrl() + "/post-song";
    const body = {
      artist: song.artist,
      title: song.title,
      trackId: song.trackId,
      genre: song.genre,
      url: song.url
    };

    return this._http.post(url, body, httpOptions);
  }
}
