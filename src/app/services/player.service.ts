import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import * as SC from 'soundcloud';

@Injectable()
export class PlayerService {
  private clientId;
  private mouseIsDown;
  public streams$: Object;

  constructor() {
    this.clientId = "iomhTXEZDUKzHt3KYi11vVWHwcdzI22s";

    SC.initialize({
      client_id: this.clientId
    });

    this.streams$ = {};
   }
   
  // seek(time){
  //   this.currentPlayer.seek(time);
  // }


  stream(trackId){
    return Observable.fromPromise(SC.stream('/tracks/' + trackId));
  };
}
