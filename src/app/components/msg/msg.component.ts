import { ApiService } from './../../services/api.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-msg',
  templateUrl: './msg.component.html',
  styleUrls: ['./msg.component.scss']
})
export class MsgComponent implements OnInit {
  public model;

  constructor(public _api: ApiService) { 
    this.model = {
      type: 'message'
    };
  }

  ngOnInit() {
  }

  onSubmit(){
    console.log(this.model);
    this._api.submitMessage(this.model).subscribe(message => {
      console.log(message);
    }, error => console.error(error));
  }
}
 