import { PlayerService } from './../../services/player.service';
import { Component, OnInit, Input } from '@angular/core';
import { DatePipe, AsyncPipe } from '@angular/common';
import * as _ from 'lodash';
@Component({
  selector: 'app-song',
  templateUrl: './song.component.html',
  styleUrls: ['./song.component.scss']
})

export class SongComponent implements OnInit {
  @Input() url: string;
  @Input() trackId: number;
  @Input() artist: string;
  @Input() title: string;

  private state;

  constructor(private _player: PlayerService) {
    this.state = 'paused';
  }

  toggle(){
    if(this._player.streams$[this.trackId].getState() === 'paused'){
      this._player.streams$[this.trackId].play();
    }else{
      _.map(this._player.streams$, (stream, id) => {
        stream.pause(); // wrong
      });
    }
  }

  isLoading() {
    return (this._player.streams$[this.trackId] && 
      this._player.streams$[this.trackId].getState() === 'loading');
  }
  
  isPlaying() {
    return (this._player.streams$[this.trackId] && 
      this._player.streams$[this.trackId].getState() === 'playing');
  }
  
  ngOnInit() {
    this._player.stream(this.trackId).subscribe(value => {
      this._player.streams$[this.trackId] = value;
    })
  }
}