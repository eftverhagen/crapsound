import { PlayerService } from './../../services/player.service';
import { Component, OnInit, Input } from '@angular/core';
import * as SC from 'soundcloud';

declare global {
  interface Window { Player: any; }
}

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss']
})

export class PlayerComponent implements OnInit {
  private clientId;
  private currentPlayer;
  private mouseIsDown;
  private duration;

  constructor(public _player: PlayerService) { 

  }

  play(){

  }

  onMouseDown(event){
    this.mouseIsDown = true;
  }

  onMouseMove(event) {
    console.log(this.duration = this.currentPlayer.getDuration());
    if(this.mouseIsDown) {
      console.log(this.currentPlayer.seek());
      this.currentPlayer.seek();
    }
  }

  ngOnInit() {

  }

}
