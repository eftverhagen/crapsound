import { PlayerService } from './../../services/player.service';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { Component, OnInit, AfterViewChecked,  } from '@angular/core';
import { MultiLevelMenuService } from '../../services/multi-level-menu.service';
import * as _ from 'lodash';
import { MusicService } from '../../services/music.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit, AfterViewChecked {
  public path;
  public embedUrl;

  constructor(
    public _menu: MultiLevelMenuService, 
    public _music: MusicService,
    public _player: PlayerService, 
    public router: Router) {
  }

  ngOnInit() {
    
  }

  ngAfterViewChecked() {
    this.router.events.subscribe((val) => {     
      if(val instanceof NavigationEnd) {
        if(val.url !== "/"){
          let unformatted = val.url;
          let splitted = unformatted.split('/');
          let firstRemoved = splitted.splice(1, splitted.length);
          this.path = firstRemoved.join(' > ');
        }else{
          this.path = '';
        }
        
      }
  });
  }
}
