import { AuthGuard } from './app.auth';
import { SoundcloudApiComponent } from './pages/soundcloud-api/soundcloud-api.component';
import { MusicComponent } from './pages/music/music.component';
import { SoftwareComponent } from './pages/software/software.component';
import { Routes, CanActivate } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';

// Routes

export const appRoutes: Routes = [
    { path: '', pathMatch: "full", component: HomeComponent},
    { path: 'software', component: SoftwareComponent},
    { path: 'music', component: MusicComponent},
    { path: 'music/:genre', component: MusicComponent},
    { path: 'add-song', component: SoundcloudApiComponent},
  ];