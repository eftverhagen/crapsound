import { ApiService } from './services/api.service';
import { SongComponent } from './components/song/song.component';
import { PlayerService } from './services/player.service';
import { PlayerComponent } from './components/player/player.component';
import { AuthGuard, Permissions, UserToken } from './app.auth';
import { appRoutes } from './app.routes';
import { AuthService } from './services/auth.service';
import { Observable } from 'rxjs/Observable';
import { HttpClientModule } from '@angular/common/http';
import { SoundcloudApiService } from './services/soundcloud-api.service';
import { SoundcloudApiComponent } from './pages/soundcloud-api/soundcloud-api.component';

import { MusicService } from './services/music.service';

import { CompanyEffects } from './effects/company.effects';
import { MultiLevelMenuService } from './services/multi-level-menu.service';
import { MultiLevelMenuComponent } from './components/multi-level-menu/multi-level-menu.component';
import { HeaderComponent } from './components/header/header.component';
import { AppComponent } from './app.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injectable } from '@angular/core';

// router stuff
import { RouterModule, Routes, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

// @ngrx stuff
import { StoreModule } from '@ngrx/store';
import { rootReducer } from './store/rootReducer';
import { EffectsModule } from '@ngrx/effects';

import { SidebarComponent } from './sidebar/sidebar.component';
import { MusicComponent } from './pages/music/music.component';
import { HomeComponent } from './pages/home/home.component';

import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer} from '@angular/platform-browser';
import { CanActivate } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { MsgComponent } from './components/msg/msg.component';

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false }
    ),
    BrowserModule,
    BrowserAnimationsModule,
    StoreModule.forRoot(rootReducer),
    HttpClientModule,
    FormsModule
    //EffectsModule.forRoot(CompanyEffects)
  ],
  declarations: [
    AppComponent,
    MultiLevelMenuComponent,
    HeaderComponent,
    SidebarComponent,
    MusicComponent,
    HomeComponent,
    SoundcloudApiComponent,
    PlayerComponent,
    SongComponent,
    MsgComponent
  ],
  providers: [
    MultiLevelMenuService,
    MusicService,
    SoundcloudApiService,
    PlayerService,
    AuthService,
    AuthGuard,
    Permissions,
    UserToken,
    ApiService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
