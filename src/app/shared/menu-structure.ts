export const Structure = [
    {id: 0, name: 'Home', items: [ // Level 1
        {name: 'Home', route: ''}, // Route
        {name: 'Software', route: 'software'}, // Route
        {name: 'Music', toggle: 2},
    ]},
    {id: 2, name: 'Music', items: [ 
        {name: 'All ', route: 'music/all'}, 
        {name: 'By Genre', toggle: 3},
        {name: 'Add Song', route: 'add-song'},
    ]},
    {id: 3, name: 'Music by genre', items: [
        {name: 'Drum and Bass', route: 'music/drum-and-bass'},
        {name: 'Psy Trance', route: 'music/psy-trance'},
        {name: 'Dubstep', route: 'music/dubstep'},
        {name: 'Hardstyle', route: 'music/hardstyle'},
        {name: 'Hardcore', route: 'music/hardcore'},
        {name: 'Tekno', route: 'music/tekno'},
    ]},
];