import { Component } from '@angular/core';
import { MultiLevelMenuService } from './services/multi-level-menu.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(public _menu: MultiLevelMenuService) {

  }
  title = 'app';
}
