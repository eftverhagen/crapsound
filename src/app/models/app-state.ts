import { Counter } from './counter';

export interface AppState {
    counter: Counter
}
