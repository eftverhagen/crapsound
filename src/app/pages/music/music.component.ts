import { Structure } from './../../shared/menu-structure';
import { MultiLevelMenuService } from './../../services/multi-level-menu.service';
import { MusicService } from './../../services/music.service';
import { Component, OnInit, ElementRef, ViewChild, AfterViewInit, AfterViewChecked } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RouterLink } from '@angular/router';
import { NgModel } from '@angular/forms';

import * as _ from 'lodash';

@Component({
  selector: 'app-music',
  templateUrl: './music.component.html',
  styleUrls: ['./music.component.scss']
})

export class MusicComponent implements OnInit, AfterViewInit, AfterViewChecked{

  @ViewChild('searchBar') searchBar: ElementRef;
  
  private genre;
  private music;
  public searchTerm;
  public genres;

  constructor(
    public route: ActivatedRoute, 
    public _menu: MultiLevelMenuService,
    public _music: MusicService,
    public _musicService: MusicService, 
    private elementRef: ElementRef,) {

    this.searchTerm = '';
    this.music = [];
    this.genres = Structure.filter(level =>level.id === 3)[0].items;    
  }

  ngOnInit(){
    this.route.params.subscribe(params => {
      this.genre = params['genre'];
      this.searchBar.nativeElement.focus();
    });
    // not loading initially?
    this._musicService.getMusic().subscribe(data => {
      this.music = data;
    });
  }

  getMusic(){
    return _.filter(this.music, song => {
      return (song.genre === this.genre || this.genre === 'all') &&
        (_.includes(song.title.toLowerCase(), this.searchTerm.toLowerCase()) ||
        _.includes(song.artist.toLowerCase(), this.searchTerm.toLowerCase()) ||
        this.searchTerm === '');
    });
  }

  ngAfterViewChecked(){
    // if(!this._menu.state[0].active){
    //   let x = window.scrollX, y = window.scrollY;
    //   this.searchBar.nativeElement.focus();
    //   window.scrollTo(x, y);
    // }
  }

  ngAfterViewInit(){
    this.searchBar.nativeElement.value = this.searchTerm;
  }
}
