import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SoundcloudApiComponent } from './soundcloud-api.component';

describe('SoundcloudApiComponent', () => {
  let component: SoundcloudApiComponent;
  let fixture: ComponentFixture<SoundcloudApiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SoundcloudApiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SoundcloudApiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
