import { AuthService } from './../../services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { element } from 'protractor';
import { SoundcloudApiService } from './../../services/soundcloud-api.service';
import { Component, OnInit } from '@angular/core';
import { NgModel } from '@angular/forms';
import * as SC from 'soundcloud';

@Component({
  selector: 'app-soundcloud-api',
  templateUrl: './soundcloud-api.component.html',
  styleUrls: ['./soundcloud-api.component.scss']
})

export class SoundcloudApiComponent implements OnInit {
  public model;
  public trackId;
  private clientId;
  private track;

  constructor(
    public _soundcloudApi: SoundcloudApiService, 
    private route: ActivatedRoute, 
    private _router: Router,
    private _auth: AuthService) { 
    this.model = {};
    
    SC.initialize({
      client_id: 'iomhTXEZDUKzHt3KYi11vVWHwcdzI22s'
    });
  }

  ngOnInit() {

  }

  publish(){
    this._soundcloudApi.resolve(this.model.url).subscribe(track => {
      this._soundcloudApi.publish(Object.assign(this.model, {
        trackId: track['id']
      })).subscribe(
        data => { console.log('succes!', data) 
      },
        error => { console.error(error) }
      )
    });
  }
}
