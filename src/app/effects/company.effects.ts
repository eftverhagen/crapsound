import { Injectable } from '@angular/core';
import { Effect } from '@ngrx/effects';
import '@rxjs/add/operator/switchmap';

@Injectable()
export class CompanyEffects {

  constructor() { }

  @Effect() loadCompanies$ = [];
}
